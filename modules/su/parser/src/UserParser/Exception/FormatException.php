<?php

namespace Su\UserParser\Exception;

use Exception;

/**
 * Class FormatException
 * @package Su\UserParser\Exception
 */
class FormatException extends Exception
{

}
