<?php
namespace App;

use Su\UserParser\Parser\CsvParser;
use Su\UserParser\Parser\JsonParser;
use Su\UserParser\UserParser;
use Exception;

include __DIR__.'/bootstrap.php';

$filename = array_key_exists('file', $_GET) ? $_GET['file'] : null;

if (null === $filename) {
    exit('Please provide filename in query string ?filename=...');
}

try {
    $parser = new UserParser();
    $parser->addParser(new CsvParser());
    $parser->addParser(new JsonParser());

    $users = $parser->parse(__DIR__.'/'.$filename);
} catch (Exception $e) {
    exit($e->getMessage());
}

?>

<table>
    <thead>
    <tr>
        <td>Login</td>
        <td>Email</td>
        <td>FirstName</td>
        <td>LastName</td>
    </tr>
    </thead>
    <tbody>
    <? foreach ($users as $user): ?>
        <tr>
            <td><? print $user->getLogin(); ?> </td>
            <td><? print $user->getEmail(); ?> </td>
            <td><? print $user->getFirstName(); ?> </td>
            <td><? print $user->getLastName(); ?> </td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>
