<?php

namespace Su\UserParser;

use Iterator;

/**
 * Interface UserLoaderInterface
 * @package Su\Loader\UserParser
 */
interface UserParserInterface {

    /**
     * @param string $filename
     * @return bool
     */
    public function supports(string $filename): bool;

    /**
     * @param string $filename
     * @return Iterator
     */
    public function parse(string $filename): Iterator;
}
