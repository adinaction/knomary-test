<?php

namespace Su\UserParser\Parser;

use Su\UserParser\Exception\FormatException;
use Su\UserParser\User;
use Su\UserParser\UserParserInterface;
use Iterator;

/**
 * Class JsonParser
 * @package Su\UserParser\Parser
 */
class JsonParser implements UserParserInterface
{

    /**
     * @param string $filename
     * @return bool
     */
    public function supports(string $filename): bool
    {
        return pathinfo($filename, PATHINFO_EXTENSION) === 'json';
    }

    /**
     * @param string $filename
     * @return Iterator
     * @throws FormatException
     */
    public function parse(string $filename): Iterator
    {
        $content = file_get_contents($filename);
        $json    = json_decode($content, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new FormatException(sprintf('Unable to read json format %s', $filename));
        }

        foreach ($json as $item) {
            $user = new User();
            $user->setLogin($item['login'] ?? null);
            $user->setEmail($item['email'] ?? null);
            $user->setFirstName($item['firstname'] ?? null);
            $user->setLastName($item['lastname'] ?? null);

            yield $user;
        }
    }
}
