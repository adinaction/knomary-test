<?php

namespace Su\UserParser\Parser;

use Su\UserParser\Exception\FileException;
use Su\UserParser\Exception\FormatException;
use Su\UserParser\User;
use Su\UserParser\UserParserInterface;
use Iterator;
use Exception;

/**
 * Class CsvParser
 * @package Su\UserParser\Parser
 */
class CsvParser implements UserParserInterface
{

    private static $map = [
        'login' => null,
        'firstname' => null,
        'lastname' => null,
        'email' => null,
    ];

    /**
     * @var string
     */
    private $delimiter;

    /**
     * CsvParser constructor.
     * @param string $delimiter
     */
    public function __construct(string $delimiter = ',')
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @param string $filename
     * @return bool
     */
    public function supports(string $filename): bool
    {
        return pathinfo($filename, PATHINFO_EXTENSION) === 'csv';
    }

    /**
     * @param string $filename
     * @return Iterator
     * @throws FileException
     * @throws FormatException
     * @throws Exception
     */
    public function parse(string $filename): Iterator
    {
        $handle = fopen($filename, 'r');

        if (!$handle) {
            throw new FileException(sprintf('Unable to load file %s', $filename));
        }

        $captions = fgetcsv($handle, 0, $this->delimiter);

        if (!is_array($captions)) {
            throw new FileException(sprintf('Unable to read csv format %s', $filename));
        }

        $this->fillIndexMap($captions);

        try {
            while ($data = fgetcsv($handle, 0, $this->delimiter)) {
                $data = array_filter($data);

                if (empty($data)) {
                    continue;
                }

                $user = new User();
                $user->setLogin($data[self::$map['login']]);
                $user->setEmail($data[self::$map['email']]);
                $user->setFirstName($data[self::$map['firstname']]);
                $user->setLastName($data[self::$map['lastname']]);

                yield $user;
            }
        } catch (Exception $e) {
            throw $e;
        } finally {
            fclose($handle);
        }
    }

    /**
     * @param array $row
     * @throws FormatException
     */
    private function fillIndexMap(array $row): void
    {
        foreach (self::$map as $field => $value) {
            $index = array_search($field, $row);

            if (false === $index) {
                throw new FormatException(sprintf('CSV has wrong format. Field %s does not exists', $field));
            }

            self::$map[$field] = $index;
        }
    }
}
