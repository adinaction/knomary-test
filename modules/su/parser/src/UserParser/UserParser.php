<?php

namespace Su\UserParser;

use Su\UserParser\Exception\FileException;
use RuntimeException;
use Iterator;

/**
 * Class UserParser
 * @package Su\Loader\UserParser
 */
class UserParser {

    /**
     * @var UserParserInterface[]
     */
    private $parsers;

    /**
     * UserParser constructor.
     */
    public function __construct()
    {
        $this->parsers = [];
    }

    /**
     * @param UserParserInterface $parser
     */
    public function addParser(UserParserInterface $parser): void
    {
        $parserName = get_class($parser);

        if (array_key_exists($parserName, $this->parsers)) {
            throw new RuntimeException(sprintf('Parser %s already added', $parserName));
        }

        $this->parsers[$parserName] = $parser;
    }

    /**
     * @param string $filename
     * @return Iterator|UserInterface[]
     * @throws FileException
     */
    public function parse(string $filename): Iterator
    {
        if (!file_exists($filename)) {
            throw new FileException(sprintf('File %s not found', $filename));
        }

        foreach ($this->parsers as $parser) {
            if (!$parser->supports($filename)) {
                continue;
            }

            return $parser->parse($filename);
        }

        throw new FileException(sprintf('File type %s not supported', $filename));
    }
}
