<?php

spl_autoload_register(
    function ($filename) {
        $filename = str_replace("\\", '/', $filename);
        $filename = __DIR__.'/modules/'.str_replace('Su/UserParser', 'su/parser/src/UserParser', $filename).'.php';

        if (file_exists($filename)) {
            /** @noinspection PhpIncludeInspection */
            require_once $filename;
        }
    }
);
