
## Basic Usage

```php
<?php

use Su\UserParser\Parser\CsvParser;
use Su\UserParser\Parser\JsonParser;
use Su\UserParser\UserParser;

$parser = new UserParser();
$parser->addParser(new CsvParser());
$parser->addParser(new JsonParser());

$users = $parser->parse('~/users.csv');
```

Full example see in index.php
