<?php

namespace Su\UserParser;

/**
 * Interface UserInterface
 * @package Su\Loader\UserParser
 */
interface UserInterface {

    /**
     * @return string|null
     */
    public function getLogin(): ?string;

    /**
     * @param string|null $login
     */
    public function setLogin(?string $login): void;

    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void;

    /**
     * @return string|null
     */
    public function getLastName(): ?string;

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void;

    /**
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void;
}
